package Final;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Implementation of the WebServer
 * 
 */

public class Server  {
	
	private static final int port = 6789;
	private ExecutorService threadPool;

	/** Main method */
	public static void main(String[] args) {
		try {
			@SuppressWarnings("unused")
			Server s = new Server();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Constructor for the WebServer
	 * 
	 * @throws IOException
	 */
	public Server() throws IOException {
		
		ServerSocket sock = new ServerSocket(port);
		threadPool = Executors.newCachedThreadPool();
		
		// Listening...
		while (true) {
			Socket client = null;

			try {
				// Found a connection
				client = sock.accept();
				assert client != null: "Error: Client is null";
				
				System.out.println("Client found: " + client);
				System.out.println("Assigning a thread...");
				
				// Assign a thread and execute
				this.threadPool.execute(new ServiceThread(client));
			} catch (Exception ioe) {
					System.err.println(ioe);
					break;
			}
		}
		// Close up shop...
		sock.close();
	}

}
