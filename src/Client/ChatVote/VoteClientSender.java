package Client.ChatVote;

import java.io.*;
import java.net.*;

/**
 * This allows the client to send information to the Vote Server.
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class VoteClientSender {
	

	@SuppressWarnings("unused")
	private Socket socket;
	private BufferedWriter writer;
	
	/**
	 * Default constructor
	 * @param socket
	 * 		Vote Server socket
	 */
	public VoteClientSender(Socket socket){
		
		this.socket = socket;
				
		try{
			this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream( )));
				
		}catch(Exception e){
			System.err.println("failed to connect to server from client sender");
			System.err.println(e);
		}
	} 

	/**
	 *	Send a message to the server.
	 * @param line
	 * 		The message send as a string.
	 */
	public void sendMessage(String line){
		
		try{
			writer.write(line + "\r\n");
			writer.flush();
		}catch(Exception e){
			System.err.println("failed to send from client sender");
			System.err.println(e);
		}
	}
	
	/**
	 * Shut down the connection between client and vote server
	 */
	public void shutDown(){
		try{
			this.writer.close();
		}catch(Exception e){
			System.err.println("failed to close client Sender");
			System.err.println(e);
		}
	}


}