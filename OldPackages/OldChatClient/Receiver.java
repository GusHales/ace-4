package ChatClient;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;


public class Receiver implements Runnable {
	
	private Socket socket;
	private BufferedReader reader;
	private boolean running = true;
	
	public Receiver(Socket socket){
		this.socket = socket;
		init();
	}

	private void init(){
		try{
			this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream( )));
			
		}catch(Exception e){
			System.out.println(e);
			running = false;
		}
		
		
	}
	@Override
	public void run() {
		
		String lineIn = "";
		
		while(running){
			
			try{
				while ((lineIn = reader.readLine( )) != null) {
		    		 System.out.println(lineIn);

		         }
				
			}catch(Exception e){
				System.out.println(e);
				running = false;
			}	
       	 
		}
		
		
		try {
			this.reader.close();
		} catch (Exception e) {
			System.out.println(e);
			running = false;
		}
		
	}
	
	public void stop(){
		this.running = false;
	}
	
	

}
