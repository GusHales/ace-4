package Server.VoteServer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * Handle the connection between the vote server and client.
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class ConnectionHandler implements Runnable {
	private Socket 		socket;
	private VoteServer 	server;
	private boolean 	running = true;
	
	private  BufferedWriter writer = null;

	public ConnectionHandler(Socket socket,VoteServer server){
		this.socket = socket;
		this.server = server;
		
	}
	
	@Override
	public void run() {
		
		while(running){
			try{
			InputStream socketIn = socket.getInputStream();
		    BufferedReader reader = new BufferedReader(new InputStreamReader(socketIn));
		    this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream( )));
		    
		    String line;
		    // Send up Date
		    sendVoteUpdate();
	        while ((line = reader.readLine()) != null)
	        {	
	            processLine(line);
	        }
		    
			}catch(Exception e){
				
			}
		}
	}
	
	/**
	 * Parse the passed line.
	 * @param line
	 * @return boolean.
	 */
	private boolean processLine(String line){
		if(line == null){ return false;}
	
		String[] tokens = line.split(":");

		if(tokens[0] == null){ return false;}
		
		if(tokens[0].equals("001"))		{sendVideoOne();}
		else if(tokens[0].equals("002")){sendVideoTwo();}
		else if(tokens[0].equals("003")){sendVideoThree();}
		else if(tokens[0].equals("004")){sendVideoFour();}
		else if(tokens[0].equals("005")){sendVideoFive();}
		
		else if(tokens[0].equals("006")){sendTopOne();}
		else if(tokens[0].equals("007")){sendTopTwo();}
		else if(tokens[0].equals("008")){sendTopThree();}
		else if(tokens[0].equals("009")){sendTopFour();}
		else if(tokens[0].equals("010")){sendTopFive();}
		
		else if(tokens[0].equals("011")){voteOne();}
		else if(tokens[0].equals("012")){voteTwo();}
		else if(tokens[0].equals("013")){voteThree();}
		else if(tokens[0].equals("014")){voteFour();}
		else if(tokens[0].equals("015")){voteFive();}
		
		else if(tokens[0].equals("102")){sendVoteUpdate();}
		else if(tokens[0].equals("200")){close();}

		return true;
	}
	
	/**
	 * Send message.
	 * @param line
	 *  The message to be sent.
	 */
	private void sendMessage(String line){
		try{
			if(writer != null){
				writer.write(line+"\r\n");
				writer.flush();
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//** Send all parsed information
	private void sendVideoOne()		{ sendMessage("001:"+server.getVideoOne());}
	private void sendVideoTwo()		{ sendMessage("002:"+server.getVideoTwo());}
	private void sendVideoThree()	{ sendMessage("003:"+server.getVideoThree());}
	private void sendVideoFour()	{ sendMessage("004:"+server.getVideoFour());}
	private void sendVideoFive()	{ sendMessage("005:"+server.getVideoFive());}
	
	private void sendTopOne()		{ sendMessage("006:"+server.getTopOne());}
	private void sendTopTwo()		{ sendMessage("007:"+server.getTopTwo());}
	private void sendTopThree()		{ sendMessage("008:"+server.getTopThree());}
	private void sendTopFour()		{ sendMessage("009:"+server.getTopFour());}
	private void sendTopFive()		{ sendMessage("010:"+server.getTopFive());}
	
	private void voteOne()			{server.voteVideoOne();}
	private void voteTwo()			{server.voteVideoTwo();}
	private void voteThree()		{server.voteVideoThree();}
	private void voteFour()			{server.voteVideoFour();}
	private void voteFive()			{server.voteVideoFive();}
	
	public void sendVoteReset()		{sendMessage("101:"+"Reset");}
	public void sendVoteUpdate()	{ 
		sendTopOne();
		sendTopTwo();
		sendTopThree();
		sendTopFour();
		sendTopFive();
	}

	/**
	 * Close the writer buffer.
	 */
	public void close() { 
		try {
			this.writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.running = false;
		this.server.removeMe(this);
	}
	
	/**
	 * Update the view with new video.
	 */
	public void sendVideoUpdate(){
		this.sendMessage("101: update video");
	}
	
}
