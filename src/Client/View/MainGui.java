package Client.View;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.io.IOException;
import java.net.Socket;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import Client.ChatVote.ClientReceiver;
import Client.ChatVote.ClientSender;
import Client.ChatVote.VoteClientReceiver;
import Client.Controller.MainListener;
import Client.Controller.VoteListener;

/**
 * The main gui.
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class MainGui implements ListSelectionListener, Runnable {

	// GUI
	private JFrame frame;
	private JMenuBar menuBar;
	private JTextField textField;
	private JTextArea textArea;
	private static JLabel poss1Label, poss2Label, poss3Label, poss4Label, poss5Label;
	private JLabel poss1id, poss2id, poss3id, poss4id, poss5id;
	private String poss1Name, poss2Name, poss3Name, poss4Name, poss5Name;
	private EmbeddedMediaPlayerComponent ui;
	private JList<String> list;
	private DefaultListModel<String> listModel;
	private JButton voteButton;
	
	//Controllers
	private MainListener mainListener;
	private VoteListener voteListener;
	
	private String IPADDRESS ;

	// IRC components
	private String IRCSERVER ;
	private final static int IRCPORT = 6667;		// Should be 6667
	private Socket IRCSocket;
	private ClientSender clientChatSender;
	private ClientReceiver clientReceiver;

	// Vote components
	private String VOTESERVER ;
	private final static int VOTEPORT = 6789;		// Should be 6667
	private Socket voteSocket;
	private ClientSender clientVodeSender;
	private String nick;
	private VoteClientReceiver voteReceiver;	
	
	/**
	 * Default constructor
	 * @param n
	 * 		The name of the user
	 */
	public MainGui(String n) {
		// Initialize
		IPLoader loader = new IPLoader();
		this.IPADDRESS = loader.load("IP.txt");
		System.out.println("IP :: "+this.IPADDRESS);
		IRCSERVER = IPADDRESS;
		VOTESERVER = IPADDRESS;
		
		nick = n;
		mainListener = new MainListener(this);
		voteListener = new VoteListener(this);
		this.createAndShowGUI();
	}
	
	/** Getters and setters */ 
	public JTextField getTextField(){
		return textField;
	}

	public JTextArea getTextArea(){
		return textArea;
	}

	public JFrame getFrame(){
		return frame;
	}
	
	public JList<String> getList(){
		return list;
	}
	
	public JButton getVoteButton(){
		return voteButton;
	}
	
	public void setPoss1Name(String s){
		poss1Label.setText(s);
	}
	
	public void setPoss2Name(String s){
		poss2Label.setText(s);
	}
	
	public void setPoss3Name(String s){
		poss3Label.setText(s);
	}
	
	public void setPoss4Name(String s){
		poss4Label.setText(s);
	}
	
	public void setPoss5Name(String s){
		poss5Label.setText(s);
	}

	
	/**
	 * Add a component to the frame
	 * @param pane
	 * 		The container panel
	 */
	private void addComponentToGui(Container pane){
		pane.setLayout(null);

		// Menu bar
		menuBar = new JMenuBar();
		JMenu file = new JMenu("File");
		JMenuItem quit = new JMenuItem("Quit");
		quit.addActionListener(mainListener);
		quit.setFocusable(false);
		file.add(quit);
		menuBar.add(file);

		// Here is where we write
		textField = new JTextField(20);
		textField.setBounds(40,500,500,25);
		textField.addActionListener(mainListener);

		// Here is where it gets displayed
		textArea = new JTextArea(5, 20);
		textArea.setBounds(40,290,500,200);
		textArea.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setBounds(40,290,500,200);

		// Add Components to this panel.
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = GridBagConstraints.REMAINDER;

		c.fill = GridBagConstraints.HORIZONTAL;
		frame.add(textField, c);

		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.weighty = 1.0;
		frame.add(scrollPane, c);


		poss1id = new JLabel("1");
		poss1id.setBounds(600,15,550,25);
		poss2id = new JLabel("2");
		poss2id.setBounds(600,65,550,25);
		poss3id = new JLabel("3");
		poss3id.setBounds(600,115,550,25);
		poss4id = new JLabel("4");
		poss4id.setBounds(600,165,550,25);
		poss5id = new JLabel("5");
		poss5id.setBounds(600,215,550,25);

		poss1Label = new JLabel(poss1Name);
		poss1Label.setBounds(650,15,550,25);
		poss2Label = new JLabel(poss2Name);

		poss2Label.setBounds(650,65,550,25);
		poss3Label = new JLabel(poss3Name);
		poss3Label.setBounds(650,115,550,25);
		poss4Label = new JLabel(poss4Name);
		poss4Label.setBounds(650,165,550,25);
		poss5Label = new JLabel(poss5Name);
		poss5Label.setBounds(650,215,550,25);

		//TODO: Add in random selections to this list from a database
		listModel = new DefaultListModel<String>();
		listModel.addElement("Waking dead");
		listModel.addElement("Men In Black");
		listModel.addElement("Miami");
		listModel.addElement("Gettin' Jiggy Wit It");
		listModel.addElement("Switch");

		// Create the list and put it in a scroll pane.
		list = new JList<String>(listModel);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setSelectedIndex(0);
		list.addListSelectionListener(this);
		list.setVisibleRowCount(5);
		JScrollPane listScrollPane = new JScrollPane(list);
		listScrollPane.setBounds(600,310,150,200);
		listScrollPane.setEnabled(false);

		voteButton = new JButton("Vote");
		voteButton.setBounds(785,385,100,50);
		voteButton.setActionCommand("Vote");
		voteButton.addActionListener(voteListener);
		voteButton.setEnabled(false);

		frame.add(voteButton);
		frame.add(listScrollPane);

		frame.add(poss1Label);
		frame.add(poss2Label);
		frame.add(poss3Label);
		frame.add(poss4Label);
		frame.add(poss5Label);

		frame.add(poss1id);
		frame.add(poss2id);
		frame.add(poss3id);
		frame.add(poss4id);
		frame.add(poss5id);

		//Video streamer
		ui = new EmbeddedMediaPlayerComponent();
		ui.setBounds(40,25,500,210);
		new NativeDiscovery().discover();
		frame.add(ui);
		
		setUpChatClient();
		setUpVote();
		sendVoteAMessage("102: Ask for update");
		String n = "NICK " + nick;
		this.sendMessage(n);
		String join = "JOIN #VideoJuke";
		this.sendMessage(join);
	}

	/**
	 * Create and show the GUI
	 */
	private void createAndShowGUI() {
		frame = new JFrame("VideoJuke");

		frame.addWindowListener(mainListener);
		
		//Set up the content pane.
		addComponentToGui(frame.getContentPane());
		frame.setFocusable(false);
		frame.setJMenuBar(menuBar);
		Container cp = frame.getContentPane();

		cp.setLayout(null);

		frame.pack();
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setLocation(200, 200);
		frame.setSize(1000, 625);
		frame.setResizable(true);
		playVideo();
	}
	
	public void windowClose(){

	}

	/*****************************************************
	 * 					CHAT CLIENT						 *
	 * 													 *
	 *****************************************************/
	/**
	 * Set up the chat client
	 */
	private void setUpChatClient(){
		setUpIRCServerSocket();
		setUpChatSender();
		setUpChatReceiver();
	}

	/**
	 * Set up the IRC socket
	 * @return bool
	 */
	private boolean setUpIRCServerSocket(){
		try{
			this.IRCSocket = new Socket(IRCSERVER, IRCPORT);
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Set up the sender
	 * @return bool
	 */
	private boolean setUpChatSender(){
		this.clientChatSender = new ClientSender(this.IRCSocket);
		return true;
	}

	/**
	 * Set up the receiver
	 * @return bool
	 */
	private boolean setUpChatReceiver(){
		clientReceiver = new ClientReceiver (this.IRCSocket, this.getTextArea());
		Thread t = new Thread(clientReceiver);
		t.start();
		return true;
	}

	/**
	 * Send a message to chat
	 * @param line
	 * 		The message we want to send
	 * @return bool
	 */
	public boolean sendMessage(String line){
		this.clientChatSender.sendMessage(line);
		return true;
	}
	
	/**
	 * Close up shop in chat
	 */
	public void shutdownChat() {
		sendMessage("QUIT");
		this.clientChatSender.shutDown();
		this.clientReceiver.shutDown();
		
		try {
			this.IRCSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*****************************************************
	 * 					VOTE CLIENT						 *
	 * 													 *
	 *****************************************************/

	/**
	 * Set up the voting client
	 */
	public void setUpVote(){
		setUpVoteSocket();
		setUpVoteReceiver();
		setUpVoteSender();
	}

	/**
	 * Set up the socket
	 * @return boolean
	 */
	public boolean setUpVoteSocket(){
		try {
			this.voteSocket = new Socket(VOTESERVER, VOTEPORT);
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true; 
	}

	/**
	 * Set up vote receiver
	 */
	public void setUpVoteReceiver(){
		voteReceiver = new VoteClientReceiver(this.voteSocket,this);
		Thread t = new Thread(voteReceiver);
		t.start();
	}
	
	/**
	 * Set up vote sender
	 * @return boolean
	 */
	private boolean setUpVoteSender(){
		this.clientVodeSender = new ClientSender(this.voteSocket);
		return true;
	}

	/**
	 * Send the vote message to the server
	 */
	public void sendVoteAMessage(String line){
		this.clientVodeSender.sendMessage(line);
	}

	/**
	 * Shutdown connection from clinet to vote server
	 */
	public void shutdownVote(){
		sendVoteAMessage("200: leavling");
		this.clientVodeSender.shutDown();
		this.voteReceiver.shutDown();
		try {
			this.voteSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Play video and display to client
	 */
	public void playVideo(){
		ui.getMediaPlayer().playMedia("rtsp://@"+IPADDRESS+":5555/demo");
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getValueIsAdjusting() == false) {

			if (list.hasFocus()) {
				voteButton.setEnabled(true);

			} else 
				voteButton.setEnabled(false);
		}
	}

	/**
	 * Update the labels
	 */
	public void upDateLabel(){
		sendVoteAMessage("006:");
		sendVoteAMessage("007:");
		sendVoteAMessage("008:");
		sendVoteAMessage("009:");
		sendVoteAMessage("010:");
	}
	

	@Override
	public void run() {
		this.createAndShowGUI();
	}
}