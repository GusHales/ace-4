package Server.ChatServer;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * Connection between the chat and client.
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class Connection {
	
	private Socket socket;
	private String hostname;
	private Account account;
	private Channel channel = null;
	
	private  BufferedWriter writer = null;
	
	/**
	 * Default constructor.
	 * @param socket
	 * 	The chat server socket.
	 */
	public Connection(Socket socket){
		this.socket = socket;
		this.hostname = socket.getInetAddress().getHostName();
		this.account = new Account();
		init();
	}

	/**
	 * Initialize the writing.
	 */
	private void init(){
		try{
			writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream( )));
		}catch(Exception e){
			e.printStackTrace();
			this.writer = null;
		}
	}
	
	/**
	 * Set the username.
	 * @param name
	 * 	The name of the user.
	 */
	public void setUserName(String name){
		this.account.setUserName(name);
	}
	
	/**
	 * Gets the required username.
	 * @return account.getUserName()
	 * 	Get the username that are stored in account.
	 */
	public String getUserName(){
		return this.account.getUserName();
	}
	
	/**
	 * For the future.
	 * @param password
	 */
	public void setPassword(String password){
		this.account.setPassWord(password);
	}
	
	/**
	 * For the future.
	 * @return
	 */
	public String getPassword(){
		return this.account.getPassWord();
	}
	
	/**
	 *  For the future.
	 * @param mode
	 */
	public void setMode(int mode){
		this.account.setMode(mode);
	} 
	
	/**
	 *  For the future.
	 * @return
	 */
	public int getMode(){
		return account.getMode();
	}
	
	/**
	 * Set the required account
	 * @param account
	 */
	public void setAccount(Account account){
		this.account = account;
	}
	
	/**
	 * Get required accounts.
	 * @return
	 */
	public Account getAccount(){
		return this.account;
	}
	
	/**
	 * Join the given channel.
	 * @param channel
	 * 	The channel that it will join.
	 */
	public void joinChannel(Channel channel){
		if(this.channel == null){
		this.channel = channel;
		this.channel.joinChannel(this);
		}else{
			System.err.println("Error :: Already in a channel " + toString());}
	}
	
	/**
	 * Leave current channel.
	 */
	public void leaveChannel(){
		if(this.channel != null){
			this.channel.leaveChannel(this);	
		}else{
			System.err.println("Error :: Not in a channel "+ toString());}
	}
	
	/**
	 * Get current channel.
	 */
	public Channel getChannel(){
		return channel;
	}
	
	/**
	 * Get the current host name for chat server.
	 * @return hostname
	 */	
	public String getHostName(){
		return hostname;
	}
	
	/**
	 * Get the socket of the server.
	 * @return
	 */
	public Socket getSocket(){
		return socket;
	}
	
	/**
	 * Send the reply to the client.
	 * @param line
	 * 	Message.
	 */
	public void sendReply(String line){
		try{
				if(writer != null){
					writer.write(line+"\r\n");
					writer.flush();
				}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	@Override
	public String toString(){
		String uName = 	"Null";
		String chan  = 	"Null";
		if(this.channel != null){chan = this.channel.getName();};
		if(this.account.getUserName() == null){ uName = this.account.getUserName();}
		
		return "Hostname: "+this.hostname+"\tUserName: "+uName+"\tChannel "+chan;
	}
	
	/**
	 * Close all connections.
	 */
	public void close(){
		try {
			this.writer.close();
			this.socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
