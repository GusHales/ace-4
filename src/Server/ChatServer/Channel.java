package Server.ChatServer;

import java.util.ArrayList;
import java.util.List;

/**
 * Channel for the chat server.
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class Channel {
	private final String name;
	@SuppressWarnings("unused")
	private final ChatServer server;
	private List<Connection> usersOnChannel;

	/**
	 * Default constructor
	 * @param name
	 * @param server
	 */
	public Channel(String name, ChatServer server){
		this.name = name;
		this.server = server;

		this.usersOnChannel = new ArrayList<Connection>();
	}

	/**
	 * Getter for the channel name.
	 * @return name
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * Join the channel.
	 * @param con
	 * @return boolean
	 * 	True for the connection being made or
	 * 	False for connection error.
	 */
	public boolean joinChannel(Connection con){
		if(usersOnChannel.contains(con)){
			System.out.println(con);
			System.out.println("Error:: User already in channel");
			return false;
		}else{
			usersOnChannel.add(con);
			con.sendReply("Joined the "+this.name+" channel");
			sendMessage(con.getUserName()+" Has joined you channel", con);
			return true;
		}
	}

	/**
	 * Leave the channel.
	 * @param con
	 * 	The channel connection.
	 */
	public void leaveChannel(Connection con){
		if(usersOnChannel.contains(con)){
			usersOnChannel.remove(con);
			con.sendReply("left "+this.name+" channel");
			sendMessage(con.getUserName()+" left the channel", con);
		}else{ System.out.println("Error :: User leaving channel has not in channel");}
	}

	/**
	 * Send message to client.
	 * @param line
	 * 	The message that will be sent.
	 * @param conIn
	 * 	The stream for the message.
	 */
	public void sendMessage(String line, Connection conIn){
		for(Connection con :this.usersOnChannel){
			con.sendReply(conIn.getUserName() +":: "+line);
		}
	}

	/**
	 * Count the number of users.
	 * @return userOnChannel.size()
	 * 	The number of users in the current channel.
	 */
	public int numberOfusers(){
		return usersOnChannel.size();
	}

}
