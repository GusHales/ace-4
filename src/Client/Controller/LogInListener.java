package Client.Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Client.View.LogInSystem;
import Client.View.MainGui;

/**
 * This is the listener for the log-in system
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class LogInListener implements ActionListener {

	// Variables
	private JTextField name, password;
	private JFrame frame;

	/**
	 * Default constructor.
	 * 
	 * @param n
	 * 		The name
	 * @param pass
	 * 		The password
	 */
	public LogInListener(JTextField n, JTextField pass, JFrame f) {
		name = n;
		password = pass;
		frame = f;
	}


	@Override
	public void actionPerformed(ActionEvent e) {

		// Determine which command we have to execute
		switch(e.getActionCommand()) {

		case "Submit": 

			try {
				// Get the values from the fields
				String user = name.getText();
				String p = password.getText();
				
				Connection con = DriverManager.getConnection("jdbc:mysql://23.229.128.39:3306/loginSystem", "aceadmin", "willsmith");
				Statement st = con.createStatement();
				String query = "SELECT password FROM users where username='"+ user +"'";
				ResultSet rs = st.executeQuery(query);

				if(rs.next()) {
					String dbpass = rs.getString(1);
					
					// Success on log-in, take them to the GUI
					if(dbpass.equals(p)){
						frame.setVisible(false);
						
						@SuppressWarnings("unused")
						MainGui mainGui = new MainGui(user);
					} else {
						JOptionPane.showMessageDialog(null,"Login Unsuccessful!","Error",1);
					}
					
				} else {
					JOptionPane.showMessageDialog(null,"Username not found","Error",1);
				}
				
			} catch (SQLException ex) {
				Logger.getLogger(LogInSystem.class.getName()).log(Level.SEVERE, null, ex);
				ex.printStackTrace();
			}
		break;

		case "Register": 
			try {
				// Get the values from the fields
				String user = name.getText();
				String p = password.getText();
				
				Connection con = DriverManager.getConnection("jdbc:mysql://23.229.128.39:3306/loginSystem", "aceadmin", "willsmith");
				Statement st = con.createStatement();
				String query = "SELECT username FROM users where username='"+ name +"'";
				ResultSet rs = st.executeQuery(query);
				
				if(rs.next()) {
					String dbuser = rs.getString(1);
					if(dbuser.equals(user)){
						JOptionPane.showMessageDialog(null,"Username already in use.","Error",1);
					}
					
				} else {
					Statement stmt = con.createStatement();
					// Insert the new values into the database
					String sql = "INSERT INTO users (username, password) " +
								"VALUES ('" + user + "', '"+ p +"')";
					stmt.execute(sql);
					
					JOptionPane.showMessageDialog(null,"Registration successfull","Success",JOptionPane.PLAIN_MESSAGE);
				}
			} catch (SQLException ex) {
				Logger.getLogger(LogInSystem.class.getName()).log(Level.SEVERE, null, ex);
				ex.printStackTrace();
			}
		break;
		}
	}
} 
