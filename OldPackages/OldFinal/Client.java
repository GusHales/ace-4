package Final;

import java.net.InetAddress;
import java.net.Socket;

import javax.swing.JFrame;

// suck me
public class Client {

	/** GUI */
	private JFrame frame;

	/** Networking */
	public static Socket sock;

	/**
	 * Main method
	 * @param args
	 * 		
	 */
	public static void main(String args[]) {
		//Create a Client object
		Client theClient = new Client();

		try {
			int server_port = 6789;
			String ServerHost = "localhost";
			InetAddress ServerIPAddr = InetAddress.getByName(ServerHost);

			//Establish a connection with the server
			theClient.sock = new Socket(ServerIPAddr, server_port);
		} catch (Exception e) {
			// Do nothing
		}
	}

	/**
	 * Constructor for the class.
	 */
	public Client() {
		frame = new JFrame("VideoJuke");
		frame.setVisible(true);
	}

}
