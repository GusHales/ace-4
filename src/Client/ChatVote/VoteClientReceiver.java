package Client.ChatVote;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

import Client.View.MainGui;

/**
 * This will allow the client to receive 
 * votes updates from the vote server.
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class VoteClientReceiver implements Runnable {
	
	private BufferedReader reader;
	private MainGui gui;

	private boolean running = true;
	
	/**
	 * Default constructor
	 * @param socket
	 * 		The Vote Server socket
	 * @param gui
	 * 		The client view
	 */
	public VoteClientReceiver(Socket socket, MainGui gui){
		this.gui = gui;

		try{
			this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream( )));
			
		}catch(Exception e){
			e.printStackTrace();
			running = false;
		}
	}
	
	
	@Override
	public void run() {
		
		String lineIn = "";
		
		while(running){
			
			try{
				while ((lineIn = reader.readLine( )) != null) {
					String[] tokens = lineIn.split(":");
					if(tokens[0] != null){
						if(tokens[0].equals("001")){ /**/}
						if(tokens[0].equals("002")){ /**/}
						if(tokens[0].equals("003")){ /**/}
						if(tokens[0].equals("004")){ /**/}
						if(tokens[0].equals("005")){ /**/}
						
						if(tokens[0].equals("006")){  gui.setPoss1Name(tokens[1]);}
						if(tokens[0].equals("007")){  gui.setPoss2Name(tokens[1]);}
						if(tokens[0].equals("008")){  gui.setPoss3Name(tokens[1]);}
						if(tokens[0].equals("009")){  gui.setPoss4Name(tokens[1]);}
						if(tokens[0].equals("010")){  gui.setPoss5Name(tokens[1]);}
						
						if(tokens[0].equals("101")){ gui.playVideo();}
						
					}

		         }
				
			}catch(Exception e){
				running = false;
			}	//TODO Maybe bandAID
       	 
		}
		
		// ShutDown 
		try {
			this.reader.close();
		} catch (Exception e) {
			e.printStackTrace();
			running = false;
		}
	}
	
	/**
	 * Shuts down the connection between client server.
	 */
	public void shutDown(){
		this.running = false;
	}
}
