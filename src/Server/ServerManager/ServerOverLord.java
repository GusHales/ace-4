package Server.ServerManager;
import java.util.Scanner;

import Server.ChatServer.ChatServer;
import Server.VideoServer.VideoServer;
import Server.VoteServer.VoteServer;


/**
 * Server overlord the master and commander of all servers.
 * @author Barry McAuley, David Forbes and Gus Hales
 */

public class ServerOverLord implements Runnable {
	
	private final ChatServer 	ircServer;
	private final VoteServer 	voteServer;
	private final VideoServer	videoServer;

	boolean running = true;
	
	private String dir = "/"; 
	
	/**
	 * Default constructor
	 * @param irc
	 * @param vote
	 * @param video
	 */
	public ServerOverLord(ChatServer irc, VoteServer vote, VideoServer video){
		this.ircServer 	= irc;
		this.voteServer = vote;
		this.videoServer= video;
		
	}
	

	@Override
	public void run() {
		printIntro();
		printServers();
		while(running){
			@SuppressWarnings("resource")
			Scanner reader = new Scanner(System.in);
			System.out.print(">");
			String line = reader.nextLine();
			proccessLine(line);
		}
	}
	
	/**
	 * Parse line
	 * @param lineIn
	 */
	private void proccessLine(String lineIn){
		if(lineIn != null){
			String[] tokens = lineIn.split(" ");
			
			if(tokens[0].equals("pwd"))			{ commandPwd();}
			else if((tokens[0].equals("cd"))&&(tokens.length > 1)){ commandCd(tokens[1]);}
			
			else if(dir.equals("/")){
				if(tokens[0].toLowerCase().equals("shutdown")){ commandShutdown();}
				else										  { System.out.println("Command not found");	}
			}
			else if(dir.equals("/IRCServer/")){
				if(tokens[0].equals("1"))		{ commandIrcPrintLoadedAcounts(); 			}
				else if(tokens[0].equals("2"))	{ commandIrcPrintChannels(); 				}
				else if(tokens[0].equals("3"))	{ commandIrcPrintConnections(); 			}
				else if(tokens[0].equals("4"))	{ commandIrcPrintUsersInChat();				}
				else							{ System.out.println("Command not found");	}
	
			}else if(dir.equals("/VideoServer/")){
				if(tokens[0].equals("1"))		{ commandVideoPrintStatus(); 				}
				else if(tokens[0].equals("2"))	{ commandVideoPrintVideoStatus();			}
				else							{ System.out.println("Command not found");	}
	
			}else if(dir.equals("/VoteServer/")){
				if(tokens[0].equals("1"))		{ commandVotePrintVidList();				}
				else if(tokens[0].equals("2"))	{ commandVotePrintTop();					}
				else if(tokens[0].equals("3"))	{ commandVotePrintVotes();					}
				else							{ System.out.println("Command not found");	}
			}
			else{System.out.println("Command not found");}
			
			
		}
	}
	
	/**
	 * Command typed in.
	 * @param in
	 */
	private void commandCd(String in){
		
		if(this.dir.equals("/")){
			if(in.toLowerCase().equals("ircserver"))		{ this.dir = "/IRCServer/"; 	printIRC(); }
			else if(in.toLowerCase().equals("videoserver"))	{ this.dir = "/VideoServer/";	printVideo();}
			else if(in.toLowerCase().equals("voteserver"))	{ this.dir = "/VoteServer/";	printVote();}
			else{System.out.println("not found");}
			
		}else if(in.equals("..")){
			if(this.dir.equals("/")){ System.out.println("not found");}
			else {this.dir = "/"; printServers();}
			
		}
	}
	
	/**
	 * Print working directory.
	 */
	private void commandPwd(){
		System.out.println(dir);
	}
	
	/**
	 * ASCII art intro.
	 */
	private void printIntro(){
		System.out.println("   _  _     _   _ _     _              ___       _  ");
		System.out.println(" _| || |_  | | | (_)   | |            |_  |     | |");
		System.out.println("|_  __  _| | | | |_  __| | ___  ___     | |_   _| | ");
		System.out.println(" _| || |_  | | | | |/ _` |/ _ \\/ _ \\    | | | | | |/ / _ \\");
		System.out.println("|_  __  _| \\ \\_/ / | (_| |  __/ (_) /\\__/ / |_| |   <  __/");
		System.out.println("  |_||_|    \\___/|_|\\__,_|\\___|\\___/\\____/ \\__,_|_|\\_\\___|");
		System.out.println("");
		
	}
	
	private void printServers(){
		System.out.println("Server list");
		System.out.println("-----------------------");
		System.out.println("IRCServer");
		System.out.println("VideoServer");
		System.out.println("VoteServer");
		System.out.println("shutdown");
		System.out.println("");
	}
	
	private void printIRC(){
		System.out.println("IRC Server");
		System.out.println("-----------------------");
		System.out.println("1 : Accounts Loaded ");
		System.out.println("2 : Channels");
		System.out.println("3 : Open Connections");
		System.out.println("4 : Users in chat");
	}
	
	
	private void printVideo(){
		System.out.println("Video Server");
		System.out.println("-----------------------");
		System.out.println("1 : Status");
		System.out.println("2 : Video Status");
	}
	
	private void printVote(){
		System.out.println("Vote server");
		System.out.println("-----------------------");
		System.out.println("1 : Video list");
		System.out.println("2 : Top 5");
		System.out.println("3 : Video votes");
		System.out.println("");
	}
	
	//-------------------
	// IRC
	//-------------------
	private void commandIrcPrintLoadedAcounts(){
		this.ircServer.printLoadedAccounts();
	}
	
	private void commandIrcPrintChannels(){
		this.ircServer.printChannelList();
	}
	
	private void commandIrcPrintConnections(){
		this.ircServer.printConnection();;
	}
	
	private void commandIrcPrintUsersInChat(){
		this.ircServer.printUsersInChat();
	}
	
	//-------------
	//Video server
	//-------------
	private void commandVideoPrintStatus(){
		this.videoServer.printIsVideoServerRunning();
	}
	
	private void commandVideoPrintVideoStatus(){
		this.videoServer.printVideoState();
	}

	//-------------
	// Vote server
	//-------------
	private void commandVotePrintTop(){
		this.voteServer.printTopFive();
	}
	
	private void commandVotePrintVidList(){
		this.voteServer.printVideoList();
	}
	
	private void commandVotePrintVotes(){
		this.voteServer.printVotes();
	}
	
	//-----------------
	// ShutDown Servers
	//-----------------
	private void commandShutdown(){
		System.out.println("Shutting down");
		this.running = false;
		this.ircServer.shutdown();
		this.videoServer.shutdown();
		this.voteServer.shutdown();
	}
}
