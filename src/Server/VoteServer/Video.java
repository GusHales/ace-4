package Server.VoteServer;


/**
 * Video Object
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class Video implements Comparable<Video> {

	private final String name;
	private int votes = 0;

	/**
	 * Default constructor.
	 * @param name
	 */
	public Video(String name){
		this.name = name;
	}

	/**
	 * Add a vote to the video.
	 */
	public void addVote(){
		this.votes++;
	}

	/**
	 * Rest votes.
	 */
	public void resetVotes(){
		this.votes = 0;
	}

	/**
	 * Get video name.
	 * @return name
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * Get number of votes.
	 * @return votes
	 */
	public int getVotes(){
		return this.votes;
	}

	@Override
	public int compareTo(Video vid) {
		int compareQuantity = vid.getVotes();
		return compareQuantity - this.votes ;
	}
}
