package Server.ChatServer;

/**
 * Creation of an account for each user.
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class Account {
	private String userName = null;;
	private String passWord = null;
	private int mode = 0;

	/**
	 * Empty constructor
	 */
	public Account(){
	}
	
	/**
	 * Getter for the user name.
	 * @return userName
	 * 		The username that was entered
	 */
	public String getUserName(){
		return this.userName;
	}

	/**
	 * For the future.
	 * @return password
	 */
	public String getPassWord(){
		return this.passWord;
	}

	/**
	 * Set username.
	 * @param name
	 * 	The name entered by the user.
	 */
	public void setUserName(String name){
		this.userName = name;
	}

	/**
	 * For the future.
	 * @param passWord
	 */
	public void setPassWord(String passWord){
		this.passWord = passWord;
	}

	/**
	 * For the future.
	 * @param mode
	 */
	public void setMode(int mode){
		this.mode = mode;
	}

	/**
	 * For the future.
	 * @return mode
	 */
	public int getMode(){
		return this.mode;
	}

	/**
	 * @return username, password
	 * 	This is for the object.
	 */
	public String toString(){
		return "Username: "+this.userName+" Password :"+this.passWord;
	}
}
