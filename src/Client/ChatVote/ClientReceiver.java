package Client.ChatVote;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

import javax.swing.JTextArea;


/**
 * This class allows client to recive information.
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class ClientReceiver implements Runnable {
	
	private BufferedReader reader;
	private JTextArea textBox;
	private boolean running = true;
	
	public ClientReceiver(Socket socket, JTextArea textBox){
			
		this.textBox = textBox;
		
		try{
			this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream( )));
			
		}catch(Exception e){
			running = false;
		}
	}
	
	
	@Override
	public void run() {
		
		String lineIn = "";
		while(running){
			
			try{
				while ((lineIn = reader.readLine( )) != null) {
		    		this.textBox.append(lineIn+"\n"); 
		    		//this.textBox.getTextArea().setCaretPosition(this.textBox.getTextArea().getDocument().getLength());
					//System.out.println(lineIn);
		    		//this.textBox.getText().selectAll();
		    		//mainGui.getTextArea().setCaretPosition(mainGui.getTextArea().getDocument().getLength());

		         }
			}catch(Exception e){
				System.out.println(e);
				running = false;
			}	
		}

		// ShutDown 
		try {
			this.reader.close();
		} catch (Exception e) {
			System.out.println(e);
			running = false;
		}
	}
	
	/**
	 * This will shut down the connection.
	 */
	public void shutDown(){
		this.running = false;
	}
}
