package Client.Driver;

import Client.View.LogInSystem;

/**
 * The main class for the client
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class ClientDriver {

	/**
	 * Default constructor
	 */
	public ClientDriver() { 
		LogInSystem logIn = new LogInSystem();
		Thread in = new Thread(logIn);
		in.start();
	}

	/**
	 * Main
	 * @param argv
	 * @throws Exception
	 */
	public static void main(String argv[]) throws Exception {
		@SuppressWarnings("unused")
		ClientDriver client = new ClientDriver();
	}
}
