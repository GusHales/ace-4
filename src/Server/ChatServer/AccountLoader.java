package Server.ChatServer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Loads the account.
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class AccountLoader {

	/**
	 * Empty constructor
	 */
	public AccountLoader(){	
	}

	/**
	 * Loads the accounts from a file.
	 * @param filepath
	 * @return loadedAccounts
	 * 	The accounts from the file.
	 */
	public List<Account> load(String filepath){
		List<Account> loadedAccounts = new ArrayList<Account>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(filepath));
			String line = br.readLine();

			while (line != null) {
				@SuppressWarnings("resource")
				Scanner scan  = new Scanner(line);
				String userName = scan.next();
				String password = scan.next();
				int mode = scan.nextInt();
				Account account = new Account();

				account.setUserName(userName);
				account.setPassWord(password);
				account.setMode(mode);

				loadedAccounts.add(account);

				line = br.readLine();
			}

			br.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return loadedAccounts;
	}
}
