package Client.Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JOptionPane;

import Client.View.MainGui;

/**
 * This is the controller for the main gui
 * @author Barry McAuley, David Forbes and Gus Hales
 *
 */
public class MainListener implements ActionListener, WindowListener{

	private MainGui mainGui;

	/**
	 * Default constructor
	 * @param gui
	 * 		The main gui
	 */
	public MainListener(MainGui gui){
		mainGui = gui;
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Quit":
			String message = "Quit?";
			int reply = JOptionPane.showConfirmDialog(mainGui.getFrame(), message, "Quit?", JOptionPane.YES_NO_OPTION);
			if (reply == JOptionPane.YES_OPTION) {
				mainGui.shutdownChat();
				mainGui.shutdownVote();
				System.exit(0);
			}
			else if(reply == JOptionPane.NO_OPTION){
			}
			break;			
		}

		String text = mainGui.getTextField().getText();
		mainGui.getTextField().selectAll();
		mainGui.sendMessage(text);
		mainGui.getTextArea().setCaretPosition(mainGui.getTextArea().getDocument().getLength());
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
	}

	@Override
	public void windowClosed(WindowEvent arg0) {

	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		mainGui.shutdownChat();
		mainGui.shutdownVote();
		mainGui.getFrame().dispose();
		System.exit(0);
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
	}
}
