package Client.Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Client.View.MainGui;

/**
 * Controller for the vote system.
 * @author Barry McAuley, David Forbes and Gus Hales
 *
 */
public class VoteListener implements ActionListener {
	
	private MainGui mainGui;
	
	/**
	 * Default constructor
	 * @param gui
	 * 		The main gui
	 */
	public VoteListener(MainGui gui){
		mainGui = gui;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		int index = mainGui.getList().getSelectedIndex();
		if (index == 0) {
			mainGui.sendVoteAMessage("011:");
			mainGui.upDateLabel();
			mainGui.getVoteButton().setEnabled(false); 
		} 
		else if(index == 1) {
			mainGui.sendVoteAMessage("012:");
			mainGui.upDateLabel();
			mainGui.getVoteButton().setEnabled(false); 
		}
		else if(index == 2) {
			mainGui.sendVoteAMessage("013:");
			mainGui.upDateLabel();
			mainGui.getVoteButton().setEnabled(false); 
		}
		else if(index == 3) {
			mainGui.sendVoteAMessage("014:");
			mainGui.upDateLabel();
			mainGui.getVoteButton().setEnabled(false); 
		}
		else if(index == 4) {
			mainGui.sendVoteAMessage("015:");
			mainGui.upDateLabel();
			mainGui.getVoteButton().setEnabled(false); 
		}
	}
}