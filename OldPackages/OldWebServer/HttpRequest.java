import java.io.*;
import java.net.Socket;
import java.util.StringTokenizer;

/**
 * The basic representation of a HttpRequest 
 *
 */

public final class HttpRequest implements Runnable {
	
	// Variables
	private final static String CRLF = "\r\n";
	private Socket socket;

	/**
	 * Constructor for the class
	 * 
	 * @param socket
	 * @throws Exception
	 */
	public HttpRequest(Socket socket) throws Exception 
	{
		this.socket = socket;
	}

	/**
	 * Implementation of the run library
	 * 
	 */
	public void run()
	{
		try {
			this.processRequest();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * Processes a HTTP request sent from a client.
	 * 
	 * @throws Exception
	 */
	private void processRequest() throws Exception
	{ 
		// Streams we're going to be working with
		BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		DataOutputStream out = new DataOutputStream(socket.getOutputStream());
		
		// Time to read the request
		String requestLine = in.readLine();
		
		// Get and display the header line
		String headerLine = null;
		while ( (headerLine = in.readLine()).length() != 0 ) {
			System.out.println(headerLine);
		}

		// Extract the file name from the request line
		StringTokenizer tokens = new StringTokenizer(requestLine);
		tokens.nextToken(); // Skip over the method, which should be 'GET'
		String fileName = tokens.nextToken();
		
		// Prepend "." so it knows it's in the current directory...
		fileName = "." + fileName;
		
		// Open the fileName
		FileInputStream fis = null;
		boolean fileExists = true;
		
		try {
			fis = new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			fileExists = false;
		}
		
		// Construct the response message.
		String statusLine = null;
		String contentTypeLine = null;
		String entityBody = null; 
		
		if (fileExists) {
			statusLine = "HTTP/1.0 200 OK";
			contentTypeLine = "Content-type: " + 
					contentType( fileName ) + CRLF;	
		} else {
			statusLine = "HTTP/1.0 404 Not Found";
			contentTypeLine = "Content-type: .html" + CRLF;
			entityBody = "<HTML>" + 
					"<HEAD><TITLE>Not Found</TITLE></HEAD>" +
					"<BODY>Not Found</BODY></HTML>";
		}
		
		// Send the status line.
		out.writeBytes(statusLine);
		// Send the content type line.
		out.writeBytes(contentTypeLine);
		// Send a blank line to indicate the end of the header lines.
		out.writeBytes(CRLF);
		
		if (fileExists) {
			sendBytes(fis, out);
			fis.close();
		} else {
			out.writeBytes(entityBody);
		}

		/// Close up shop
		out.close();
		in.close();
	}
	
	/**
	 * Returns the type of the content present
	 * within the file.
	 * 
	 * @param fileName
	 * @return
	 */
	private String contentType(String fileName) {
		
		if(fileName.endsWith(".htm") || fileName.endsWith(".html")) {
			return "text/html";
		}
		if(fileName.endsWith(".gif")) {
			return "image/gif";
		}
		if(fileName.endsWith(".jpeg") || fileName.endsWith(".jpg")) {
			return "image/jpeg";
		}
		return "application/octet-stream";
		
	}
	
	/**
	 * Sends bytes through the socket...
	 * 
	 * @param fis
	 * @param os
	 * @throws Exception
	 */
	private static void sendBytes(FileInputStream fis, OutputStream os)	throws Exception
	{
		// Construct a 1K buffer to hold bytes on their way to the socket.
		byte[] buffer = new byte[1024];
		int bytes = 0;
		// Copy requested file into the socket's output stream.
		while((bytes = fis.read(buffer)) != -1 ) {
			os.write(buffer, 0, bytes);
		}
	}

}


