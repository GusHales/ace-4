package Client.View;

import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Client.Controller.LogInListener;

/**
 * The log-in system which will connect to the remote
 * server in order to log into the VideoJuke system.
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class LogInSystem implements Runnable {
	
	// GUI components
	private JTextField name;
	private JPasswordField pass;
	private JButton submit, register;
	private JFrame frame;
	private JLabel username, password;
	
	@Override
	public void run() {
		createAndShowGUI();
	}

	/**
	 * Adds a component to the GUI
	 * @param pane
	 * 		The container pane
	 */
	private void addComponentToGui(Container pane){
		pane.setLayout(null);

		username = new JLabel("Username: ");
		name = new JTextField(10);
		username.setBounds(40,20,100,25);
		name.setBounds(110,20,100,25);
		password = new JLabel("Password: ");
		pass = new JPasswordField(10);
		pass.echoCharIsSet();
		password.setBounds(40,55,100,25);
		pass.setBounds(110,55,100,25);

		submit = new JButton("Submit");
		submit.setBounds(140,95,100,25);
		submit.addActionListener(new LogInListener(name, pass, frame));

		register = new JButton("Register");
		register.setBounds(20,95,100,25);
		register.addActionListener(new LogInListener(name, pass, frame));

		frame.add(username);
		frame.add(password);
		frame.add(name);
		frame.add(pass);
		frame.add(submit);
		frame.add(register);
	}

	/**
	 * Create and show the GUI
	 */
	private void createAndShowGUI() {
		frame = new JFrame("VideoJuke LogIn");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		addComponentToGui(frame.getContentPane());

		frame.setFocusable(true);

		frame.pack();
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setLocation(200, 200);
		frame.setSize(300, 180);
		frame.setResizable(true);
	}
}