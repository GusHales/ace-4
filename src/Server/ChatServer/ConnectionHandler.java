package Server.ChatServer;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Handle the connection between the chat server and client.
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class ConnectionHandler implements Runnable {
	private Connection connection;
	private ChatServer server;
	private boolean running;

	/**
	 * Default construct
	 * @param connection
	 * @param server
	 */
	public ConnectionHandler(Connection connection,ChatServer server){
		this.connection = connection;
		this.server = server;
	}

	@Override
	public void run() {
		running = true;

		while(running){
			try{
				InputStream socketIn = connection.getSocket().getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(socketIn));

				String line;
				while ((line = reader.readLine()) != null)
				{	
				processLine(line);
				}

			}catch(Exception e){
			}
		}
	}

	/**
	 * Parse the line.
	 * @param line
	 * @return boolean
	 */
	private boolean processLine(String line){
		if(line == null){ return false;}

		String[] tokens = line.split(" ");
		if(tokens.length == 1){
			if(tokens[0].equals("QUIT"))	{ commandQuit();				 return true;}
		}
		else if(tokens.length == 2 ){
			if(tokens[0].equals("JOIN"))		{ commandJOIN(tokens[1].trim()); return true;}
			else if(tokens[0].equals("NICK"))	{ commandNick(tokens[1].trim()); return true;}
			else if(tokens[0].equals("LEAVE"))	{ commandLeaveChannel();		 return true;}
		}else if(tokens.length == 3 ){
			if(tokens[0].equals("/nick"))	{ commandNickPassword(tokens[1].trim(), tokens[2].trim()); return true;}
		}
		if(connection.getChannel() == null){
			connection.sendReply("Not in a channel");
		}else if(connection.getUserName() == null){
			connection.sendReply("User name not set");
		}else{
			connection.getChannel().sendMessage(line, connection);
		}
		return true;
	}

	/**
	 * Join the given channel.
	 * @param channelName
	 */
	private void commandJOIN(String channelName){
		Channel chan = server.findChannel(channelName);
		if(connection.getChannel() != null){
			connection.leaveChannel();
		}
		if(chan != null){
			connection.joinChannel(chan);
		}else{
			connection.sendReply("Channel not found");
		}

	}
	
	/**
	 * Set nick to name.
	 * @param name
	 */
	private void commandNick(String name){
		server.setUsername(name, connection);
	}
	
	/**
	 * For the future.
	 * @param name
	 * @param password
	 */
	private void commandNickPassword(String name, String password){

		server.setUsernameWithPassWord(name, password, connection);

	}
	
	/**
	 * Leave the current channel.
	 */
	private void commandLeaveChannel(){
		if(connection.getChannel() == null){
			connection.sendReply("You are not in a channel");
		}
		else{
			connection.leaveChannel();
		}
	}

	/**
	 * Quit the channel and terminate the stream.
	 */
	private void commandQuit(){
		connection.leaveChannel();
		connection.close();
		this.running = false;
		this.server.removeConnection(this.connection);
	}
}
