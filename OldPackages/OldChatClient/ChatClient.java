package ChatClient;
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class ChatClient {
	
	private boolean running = true;
	private final static String SERVER = "127.0.0.1";
	private final static int PORT = 6666;		// Should be 6667
	private Socket socket;
	private BufferedWriter writer;
	private Scanner userInput;
	
	
	public ChatClient(){
		init();
		runLoop();	
	}
	
	private void init(){
		try{
			this.socket = new Socket(SERVER, PORT);
			this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream( )));
			this.userInput = new Scanner( System.in );
			
		}catch(Exception e){
			System.out.println(e);
			running = false;
		}
		Receiver display = new Receiver(this.socket);
		Thread t = new Thread(display);
        t.start();
        
	} 
	
	private void runLoop(){
		System.out.println("Client start");
		String line;
		while(running){
			
			try{
				System.out.print(">");
				line = userInput.nextLine();
				writer.write(line + "\r\n");
				writer.flush();
				
			}catch(Exception e){
				System.out.println(e);
				running = false;
			}
			
			// i will put if checks here
		}
		
		
		
	}

    public static void main(String[] args) {
    	ChatClient chat = new ChatClient();
    }

}