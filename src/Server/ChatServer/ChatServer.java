package Server.ChatServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * The server for the chat service.
 * @author Barry McAuley, David Forbes and Gus Hales
 *
 */
public class ChatServer implements Runnable {
	private boolean running;
	private ServerSocket serverSocket;
	private final int portNumber = 6667;

	private List<Connection> connectionList;
	private List<Channel> channelList;
	private List<Account> loadedAccounts;

	/**
	 * Default constructor.
	 */
	public ChatServer(){
		this.channelList = 		new ArrayList<Channel>();
		this.connectionList	= 	new ArrayList<Connection>();
		this.loadedAccounts =			new ArrayList<Account>();   

		// Add a channel
		addChannel("#VideoJuke");

		loadAccounts();

	}

	/**
	 * Open server socket.
	 * @return boolean
	 */
	private boolean openServerSocket(){
		try{
			this.serverSocket = new ServerSocket(portNumber);
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * Start the server socket.
	 */
	public void start(){
		if(openServerSocket()){
			running = true;

			while(running){
				try{
					Socket s = serverSocket.accept();
					Connection con = new Connection(s);
					connectionList.add(con);
					ConnectionHandler hander = new ConnectionHandler(con, this);
					Thread t = new Thread(hander);
					t.start();

				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Connection and User Handling
	 * @param name
	 * 	The name of the user
	 * @param password
	 * 	The password the user
	 * @param connection
	 * 	The connection for the user to the chat server
	 * @return boolean
	 */
	public boolean setUsernameWithPassWord(String name, String password, Connection connection){
		boolean userSet = false;
		// must be one word
		if(name.contains(" ")){ return false;};
		// max length of 16
		if(name.length() > 16){ return false;} 

		//Check if username maches password
		name = name.toLowerCase();
		for(Account account : loadedAccounts){
			if(account.getUserName().toLowerCase().equals(name)){
				if(account.getPassWord().equals(password)){
					connection.setUserName(name);
					connection.sendReply("Username set to : "+name);
					userSet = true;
				}else{  
					connection.sendReply("wrong password");
					userSet = true;
				}
			}else{

			}
		}

		if(!userSet){
			connection.sendReply("Account not found");
		}

		return true;
	}


	/**
	 * Set username.
	 * @param name
	 * 	The name for the given user.
	 * @param connection
	 * 	The connection for the given user.
	 * @return boolean
	 */
	public boolean setUsername(String name, Connection connection){
		// must be one word
		if(name.contains(" ")){ return false;};
		// max length of 16
		if(name.length() > 16){ return false;} 

		if(checkUsernameInUses(name)){
			connection.setUserName(name);
			connection.sendReply("Username set to : "+name);
		}else{
			connection.sendReply(name+" is not vaild or is taken");
		}
		return true;
	}

	/**
	 * Check that username is used -- stops double log in
	 * @param name
	 * 	The username of the logger
	 * @return boolean
	 */
	private boolean checkUsernameInUses(String name){
		name = name.toLowerCase();
		for(Account account : getAllAccounts()){

			String accountName = account.getUserName();
			if(accountName != null){
				if(account.getUserName().toLowerCase().equals(name)) {  
					return false;
				}
			}

		}
		return true;
	}

	/**
	 * Get all of the accounts.
	 * @return allAccounts
	 * 	The acounts that are stored.
	 */
	private List<Account> getAllAccounts(){
		List<Account> allAccounts = new ArrayList<Account>();
		allAccounts.addAll(this.loadedAccounts);
		for(Connection con: connectionList){
			allAccounts.add(con.getAccount());
		}
		return allAccounts;
	} 

	/**
	 * Load accounts.
	 */
	private void loadAccounts(){
		AccountLoader loader = new AccountLoader();
		this.loadedAccounts = loader.load("Accounts.txt"); 
	} 

	/**
	 * Add channel.
	 * @param name
	 * 	The channel name.
	 * @return boolean
	 */
	private boolean addChannel(String name){
		if(!channelNameInUse(name)){
			Channel chan = new Channel(name, this);
			channelList.add(chan);
			return true;
		}
		return false;
	} 

	/**
	 * Find channel requested
	 * @param channelName
	 * 	The name of the channel requested to find.
	 * @return channel
	 */
	public Channel findChannel(String channelName){
		for(Channel chan : channelList){
			if(chan.getName().toLowerCase().equals(channelName.toLowerCase())){ return chan;}
		}
		return null;
	}

	/**
	 * Channel name in use.
	 * @param name 
	 * 	The name of thechannel
	 * @return boolean
	 */
	private boolean channelNameInUse(String name){
		name = name.toLowerCase();
		Channel chan = findChannel(name);
		if(chan == null){return false;}
		return true;
	}

	/**
	 * Testing server side
	 */
	public void printChannelList(){
		System.out.println("Channel name");
		System.out.println("----------------------------------------------------");
		for(Channel chan : channelList){
			System.out.println(chan.getName() );
		}
	}

	/**
	 * Testing server side
	 */
	public void printLoadedAccounts(){
		System.out.println("UserName \tPassword \t Mode");
		System.out.println("----------------------------------------------------");
		for(Account account : loadedAccounts){
			System.out.println(account.getUserName()+"\t\t"+account.getPassWord()+"\t\t"+account.getMode() );
		}
	}

	/**
	 * Testing server side
	 */
	public void printConnection(){
		System.out.println("Open Connections");
		System.out.println("-----------------------------");
		for(Connection con : connectionList ){
			System.out.println(con);
		}
	}
	
	/**
	 * Testing server side
	 */
	public void printUsersInChat(){
		System.out.println("Users in Chat\n");
		System.out.println("Nick\t\tChannel");
		System.out.println("------------------------------");
		for(Connection con : connectionList ){
			System.out.println(con.getAccount().getUserName()+"\t\t"+con.getChannel().getName());
		}
	}

	/**
	 * Handle/remove the connection
	 * @param con
	 * 	The connection that will be removed
	 */
	public void removeConnection(Connection con){
		if(connectionList.contains(con)){
			connectionList.remove(con);
		}
	}

	@Override
	public void run() {
		start();
	}

	// ---- Shutdown ---------//
	
	public void shutdown(){
		this.running = false;
		try {
			this.serverSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}
}
