package Server.VideoServer;

import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.headless.HeadlessMediaPlayer;
import Server.VoteServer.VoteServer;

/**
 * Server for the video streaming.
 * @author Barry McAuley, David Forbes and Gus Hales
 *
 */
public class VideoServer implements Runnable {
	
	private HeadlessMediaPlayer mediaPlayer;
	private VoteServer voteServer;
	private boolean running = true;
		
	/**
	 * Default constructor
	 */
	public VideoServer(){
		
	    MediaPlayerFactory mediaPlayerFactory = new MediaPlayerFactory();
	    this.mediaPlayer = mediaPlayerFactory.newHeadlessMediaPlayer();
	    
	    this.voteServer = new VoteServer();
	    Thread t = new Thread(voteServer);
	    t.start();
	    
	}

	// Play all media
	public void playVideoOne(){
		
		mediaPlayer.stop();
		
		mediaPlayer.playMedia("TheWalkingDead.mp4", 
	    		":sout=#rtp{sdp=rtsp://@:5555/demo}",
                ":no-sout-rtp-sap", 
               ":no-sout-standard-sap", 
                ":sout-all",
                ":rtsp-caching=100",
                ":sout-keep");
	}
	
	public void playVideoTwo(){

		mediaPlayer.stop();
		
		mediaPlayer.playMedia("MenInBlack.mp4", 
	    		":sout=#rtp{sdp=rtsp://@:5555/demo}",
                ":no-sout-rtp-sap", 
               ":no-sout-standard-sap", 
                ":sout-all",
                ":rtsp-caching=100",
                ":sout-keep");
	}
	
	public void playVideoThree(){

		mediaPlayer.stop();
		
		mediaPlayer.playMedia("Miami.mp4", 
	    		":sout=#rtp{sdp=rtsp://@:5555/demo}",
                ":no-sout-rtp-sap", 
               ":no-sout-standard-sap", 
                ":sout-all",
                ":rtsp-caching=100",
                ":sout-keep");
	}
	
	public void playVideoFour(){

		mediaPlayer.stop();
		
		mediaPlayer.playMedia("GettinJiggyWitIt.mp4", 
	    		":sout=#rtp{sdp=rtsp://@:5555/demo}",
                ":no-sout-rtp-sap", 
               ":no-sout-standard-sap", 
                ":sout-all",
                ":rtsp-caching=100",
                ":sout-keep");
	}
	
	public void playVideoFive(){
		
		mediaPlayer.stop();
		
		mediaPlayer.playMedia("Switch.mp4", 
	    		":sout=#rtp{sdp=rtsp://@:5555/demo}",
                ":no-sout-rtp-sap", 
               ":no-sout-standard-sap", 
                ":sout-all",
                ":rtsp-caching=100",
                ":sout-keep");
	}
	
	
	/**
	 * Play the next video in the queue or vote.
	 */
	private void playNextVideo(){
		int var = this.voteServer.getVideoNumber();
		
		// String name = this.voteServer.getTop.getName().concat(.mp
		
		if(var == 1){playVideoOne();}
		else if(var == 2){	playVideoTwo();}
		else if(var == 3){	playVideoThree();}
		else if(var == 4){	playVideoFour();}
		else if(var == 5){	playVideoFive();}
		
		this.voteServer.resetVotes();
	}
	
	/**
	 * Update video.
	 */
	private void videoUpDate(){
		this.voteServer.videoUpdate();
	}
	
	/**
	 * Get the vote server
	 * @return voteServer
	 */
	public VoteServer getVoteServer(){
		return this.voteServer;
	}
	
	@Override
	public void run() {
		
		long reload = 20000000*5;   // 20000000 = 12 sec
		int isNotPlaying = 0;
		int target = 100000; 
		
		long preTime = 0;
		// We're running! 
		while(running){
			
			
			if(mediaPlayer.isPlaying()){
				isNotPlaying = 0;
				preTime++;
				if(preTime > reload){
					preTime = 0;
					videoUpDate();
				}
				
			}else{
				isNotPlaying++;
			}
			
			if(isNotPlaying > target){
				isNotPlaying = 0;
				playNextVideo();
			}
		}
		
	}
	

	/**
	 * Testing prints.
	 */
	public void printIsVideoServerRunning(){
		System.out.println("Video Server status");
		System.out.println("--------------------");
		if(this.running){
			System.out.println("Video server state :: RUNNING");
		}
		else{ System.out.println("Video server state :: STOPPED");}
	}
	
	/**
	 * Check the state of the video.
	 */
	public void printVideoState(){
		System.out.println("Video Status");
		System.out.println("---------------------");
		System.out.println("Time \t\tLength \t\tMuted");
		boolean mute 	= mediaPlayer.isMute();
		long time		= mediaPlayer.getTime();
		long length 	=mediaPlayer.getLength();
		String muteString;
		if(mute){muteString = "TRUE"; }else{ muteString = "FALSE";}
		
		System.out.println(time+"\t\t"+length+"\t\t"+muteString);
	}

	/**
	 * Shutdown server.
	 */
	public void shutdown(){
		this.running = false;
		this.mediaPlayer.stop();
	}
}
