package Test;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;

import uk.co.caprica.vlcj.binding.LibVlc;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.runtime.RuntimeUtil;

public class main extends JFrame{
	
	private static TestUI ui;
	
	public static void main(String[] args){
		new NativeDiscovery().discover();
		
		ui = new TestUI();
		
		main frame = new main();
		
		frame.add(ui,BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(true);
		frame.setSize(500, 500);
		
		frame.setVisible(true);
	//		String vlcHome = "dir/with/dlls"; // Dir with vlc.dll and vlccore.dll
	//        NativeLibrary.addSearchPath(
	//            RuntimeUtil.getLibVlcLibraryName(), vlcHome
	//        );
	//        Native.loadLibrary(RuntimeUtil.getLibVlcLibraryName(), LibVlc.class);
		
		
		ui.getMediaPlayer().playMedia("TheWalkingDead.mp4");
		
	}

}
