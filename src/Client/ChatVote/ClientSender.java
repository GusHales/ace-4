package Client.ChatVote;

import java.io.*;
import java.net.*;

/**
 * This allows the client to send information back.
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class ClientSender {

	@SuppressWarnings("unused")
	private Socket socket;
	private BufferedWriter writer;
	
	/**
	 * Default constructor
	 * @param socket
	 * 		The Vote Server socket
	 */
	public ClientSender(Socket socket){
		this.socket = socket;
		try{
			this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream( )));
				
		}catch(Exception e){
			System.err.println("failed to connect to server from client sender");
			e.printStackTrace();
		}
        
	} 
	
	/**
	 * Send a message to chat
	 * @param line
	 *		The message we want to send
	 */
	public void sendMessage(String line){
		try{
			writer.write(line + "\r\n");
			writer.flush();
			
		}catch(Exception e){
			System.err.println("failed to send from client sender");
			e.printStackTrace();
		}
	}

	/**
	 * This will terminate the connection
	 */
	public void shutDown(){
		try{
			this.writer.close();
		}catch(Exception e){
			System.err.println("failed to close client Sender");
			e.printStackTrace();
		}
	}


}