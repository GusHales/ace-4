package Client.View;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

/**
 * Loads the account.
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class IPLoader {

	/**
	 * Empty constructor
	 */
	public IPLoader(){	
	}

	/**
	 * Loads the accounts from a file.
	 * @param filepath
	 * @return loadedAccounts
	 * 	The accounts from the file.
	 */
	public String load(String filepath){
		String ip ="";
		try {
			BufferedReader br = new BufferedReader(new FileReader(filepath));
			String line = br.readLine();

				@SuppressWarnings("resource")
				Scanner scan  = new Scanner(line);
				ip = scan.next();

			br.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return ip;
	}
}
