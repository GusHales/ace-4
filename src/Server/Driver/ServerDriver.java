package Server.Driver;

import Server.ChatServer.ChatServer;
import Server.ServerManager.ServerOverLord;
import Server.VideoServer.VideoServer;


/**
 * Runs the servers.
 * @author Barry McAuley, David Forbes and Gus Hales
 */
public class ServerDriver {
	
	/**
	 * Default constructor
	 */
	public ServerDriver(){
		
		//-----------
		// Chat 
		//-----------
		ChatServer chatserver = new ChatServer();
		Thread chat = new Thread(chatserver);
		chat.start();
		
		//------------
		// Video
		//------------
		VideoServer videoserver = new VideoServer();
	    Thread video = new Thread(videoserver);
	    video.start();
	    
	    //----- Server manager ----
	    ServerOverLord overLord = new ServerOverLord(chatserver,videoserver.getVoteServer(),videoserver);
	    Thread lord = new Thread(overLord);
	    lord.start();
	    
		
	}
	
	public static void main(String argv[]) throws Exception
	{
		@SuppressWarnings("unused")
		ServerDriver server = new ServerDriver();
	}

}
