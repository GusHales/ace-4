package Server.VoteServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Server.VoteServer.ConnectionHandler;

public class VoteServer implements Runnable {
	
	private boolean running;
	private ServerSocket serverSocket;
	private final int portNumber = 6789;
	
	private final String VIDEOONE	= "The walking dead";
	private final String VIDEOTWO	= "Men In Black"; 
	private final String VIDEOTHREE	= "Miami"; 
	private final String VIDEOFOUR	= "Gettin' Jiggy Wit It";
	private final String VIDEOFIVE	= "Switch"; 
	
	
	private Video[] topFive;
	private List<ConnectionHandler> conList;
	
	public VoteServer(){
		
		topFive = new Video[5];
		
		Video a = new Video(this.VIDEOONE);
		Video b = new Video(this.VIDEOTWO);
		Video c = new Video(this.VIDEOTHREE);
		Video d = new Video(this.VIDEOFOUR);
		Video e = new Video(this.VIDEOFIVE);
		
		topFive[0] = a;
		topFive[1] = b;
		topFive[2] = c;
		topFive[3] = d;
		topFive[4] = e;
		
		this.conList = new ArrayList<ConnectionHandler>();
		
	}
	
	private boolean openServerSocket(){
		try{
			this.serverSocket = new ServerSocket(portNumber);
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	public void start(){
		if(openServerSocket()){
			running = true;

			while(running){
				try{
					Socket s = serverSocket.accept();

					ConnectionHandler hander = new ConnectionHandler(s, this);
					this.conList.add(hander);
					Thread t = new Thread(hander);
					t.start();

				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
	
	
	//-----------------
	// Get video name
	//-----------------
	
	public String getVideoOne()		{ return this.VIDEOONE; 	}
	public String getVideoTwo()		{ return this.VIDEOTWO; 	}
	public String getVideoThree()	{ return this.VIDEOTHREE;	}
	public String getVideoFour()	{ return this.VIDEOFOUR; 	}
	public String getVideoFive()	{ return this.VIDEOFIVE; 	}
	
	//-----------------
	// Add vote
	//-----------------
	
	public void voteVideoOne()	{
		Video vid = findVideo(this.VIDEOONE);
		vid.addVote();
		votesUpdate();
	}
	
	public void voteVideoTwo()	{
		Video vid = findVideo(this.VIDEOTWO);
		vid.addVote();
		votesUpdate();
	}
	
	public void voteVideoThree(){
		Video vid = findVideo(this.VIDEOTHREE);
		vid.addVote();
		votesUpdate();
	}
	
	public void voteVideoFour()	{
		Video vid = findVideo(this.VIDEOFOUR);
		vid.addVote();
		votesUpdate();
	}
	
	public void voteVideoFive()	{
		Video vid = findVideo(this.VIDEOFIVE);
		vid.addVote();
		votesUpdate();
	
	}
	
	private void resetAllVotes(){
		for(Video vid :topFive){
			vid.resetVotes();
		}
	}
	
	private Video findVideo(String name){
		
		for(Video vid : topFive){
			if(vid.getName().equals(name)){return vid;}
		}
		return null;
	}
	
	//-----------------
	// get top five
	//-----------------
	
	public String getTopOne(){
		Arrays.sort(topFive);
		Video vid = topFive[0];
		return vid.getName();
	}
	
	public String getTopTwo(){
		Arrays.sort(topFive);
		Video vid = topFive[1];
		return vid.getName();
	} 
	
	public String getTopThree(){
		Arrays.sort(topFive);
		Video vid = topFive[2];
		return vid.getName();
	} 
	
	public String getTopFour(){
		Arrays.sort(topFive);
		Video vid = topFive[3];
		return vid.getName();
	} 
	
	public String getTopFive(){
		Arrays.sort(topFive);
		Video vid = topFive[4];
		return vid.getName();
	} 

	
	@Override
	public void run() {
		start();
	}
	
	public int getVideoNumber(){
		Video vid = findVideo(getTopOne());
		if(vid.getName().equals(this.VIDEOONE))		{return 1;}
		if(vid.getName().equals(this.VIDEOTWO))		{return 2;}
		if(vid.getName().equals(this.VIDEOTHREE))	{return 3;}
		if(vid.getName().equals(this.VIDEOFOUR))	{return 4;}
		if(vid.getName().equals(this.VIDEOFIVE))	{return 5;}
		return -1;
		
	}
	
	public void removeMe(ConnectionHandler con){
		this.conList.remove(con);
	}
	
	public void resetVotes(){
		resetAllVotes();
		for(ConnectionHandler con : conList){
			con.sendVoteReset();
		}
	}
	
	public void votesUpdate(){
		
		for(ConnectionHandler con : conList){
			con.sendVoteUpdate();
		}
	}
	
	public void videoUpdate(){
		for(ConnectionHandler con : conList){
			con.sendVideoUpdate();
		}
	}
	
	//----- Testing and Debugging -----//
	
	public void printTopFive(){
		System.out.println("Top Five");
		System.out.println("----------------------");
		System.out.println("1 :: "+ getTopOne());
		System.out.println("2 :: "+ getTopTwo());
		System.out.println("3 :: "+ getTopThree());
		System.out.println("4 :: "+ getTopFour());
		System.out.println("5 :: "+ getTopFive());
	}
	
	public void printVideoList(){
		System.out.println("Video list");
		System.out.println("-----------------------");
		System.out.println("1 :: "+ getVideoOne());
		System.out.println("2 :: "+ getVideoTwo());
		System.out.println("3 :: "+ getVideoThree());
		System.out.println("4 :: "+ getVideoFour());
		System.out.println("5 :: "+ getVideoFive());
	}
	
	public void printVotes(){
		System.out.println("Votes\n");
		System.out.println("Name\t\t\tVotes");
		System.out.println("-------------------------");
		
		Video one 	= findVideo(getVideoOne());
		Video two 	= findVideo(getVideoTwo());
		Video three = findVideo(getVideoThree());
		Video four 	= findVideo(getVideoFour());
		Video five 	= findVideo(getVideoFive());
		
		System.out.println(one.getName()+"\t\t"+one.getVotes());
		System.out.println(two.getName()+"\t\t\t"+two.getVotes());
		System.out.println(three.getName()+"\t\t\t\t"+three.getVotes());
		System.out.println(four.getName()+"\t\t"+four.getVotes());
		System.out.println(five.getName()+"\t\t\t\t"+five.getVotes());
	}

	// ---- Shutdown ---------//
	
	public void shutdown(){
		this.running = false;
		try {
			this.serverSocket.close();
		} catch (IOException e) {
			//e.printStackTrace();
		}
	}

}
